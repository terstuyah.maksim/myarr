import MyArray from '../index';

describe('tests for method filter', () => {
  test('instance has method filter', () => {
    const arr = new MyArray(1, 4, 0);
    expect(arr.filter).toBeInstanceOf(Function);
  });

  test('result array should have correct length', () => {
    const arr = new MyArray(1, 2, 3, 4);
    const f = arr.filter(x => x < 3);
    expect(f).toHaveLength(2);
    expect(f).toEqual(new MyArray(1, 2));
  });

  test('filter should not change initial instance', () => {
    const arr = new MyArray(1, 2, 3, 4);
    arr.filter(x => x < 3);

    expect(arr).toEqual(new MyArray(1, 2, 3, 4));
  });

  test('Callback function should use optional this, if it is provided as a second argument', () => {
    const arr = new MyArray(1, 2, 3, 4);
    const mockFn = jest.fn();
    const optionalThis = { a: 1 };
    arr.filter(mockFn.mockReturnThis(), optionalThis);
    expect(mockFn).toHaveReturnedWith(optionalThis);
  });

  test('should return an empty array, if no element matches callback condition', () => {
    const arr = new MyArray(1, 2, 3, 4);
    const f = arr.filter(x => x > 6);
    expect(f).toHaveLength(0);
  });

  test('should work with elements added directly', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    arr[5] = 6;
    arr[6] = 7;
    const f = arr.filter(a => a > 4);
    expect(f).toEqual(new MyArray(5, 6, 7));
  });

  test('Filter should not work with items added "in-progress" of callback invocation', () => {
    const arr = new MyArray(0, 1, 2, 3, 4);
    const len = arr.length;
    const f = arr.filter(el => {
      arr.push(3);

      if (arr.length >= len * 2) {
        throw new Error('Test fell into recursive call of callback function');
      }
      return el > 2;
    });
    expect(f).toEqual(new MyArray(3, 4));
  });

  test('should give to callback function with 3 arguments (iteration element, index, iterable array)', () => {
    const mockFn = jest.fn();
    const arr = new MyArray('a', 'b');
    arr.filter(mockFn);
    expect(mockFn).toBeCalledWith('a', 0, arr);
  });

  test('should take 1 required argument', () => {
    expect(MyArray.prototype.filter).toHaveLength(1);
  });

  test('callback should be called n-times, where n is a number of non-empty elements', () => {
    const arr = new MyArray(1, 2, 3, undefined);
    arr[7] = 10;
    const mockFn = jest.fn();
    const f = arr.filter(mockFn);
    expect(mockFn).toHaveBeenCalledTimes(5);
  });

  test('result array should be instance of array class', () => {
    const arr = new MyArray(1, 2, 3);
    const f = arr.filter(x => x < 2);

    expect(f).toBeInstanceOf(MyArray);
  });

  test('instance has not Own Property filter', () => {
    const arr = new MyArray(1, 2, 3);
    expect(Object.prototype.hasOwnProperty.call(arr, 'filter')).toBeFalsy();
  });
});
