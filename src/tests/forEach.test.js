import MyArray from '../index';

describe('tests for method forEach', () => {
  let arr = new MyArray();
  beforeEach(() => {
    arr = new MyArray(1, 2, 3, 4, 5);
  });
  test('should return undefined', () => {
    expect(arr.forEach(item => item)).toBeUndefined();
  });
  test('should not work with empty elements ', () => {
    delete arr[2];
    const mockCallback = jest.fn();
    arr.forEach(mockCallback);
    expect(mockCallback.mock.calls).toHaveLength(4);
  });
  test('should work with elements which value equal to undefined, NaN, null', () => {
    arr = new MyArray(undefined, null, NaN);
    const mockCallback = jest.fn();
    arr.forEach(mockCallback);
    expect(mockCallback.mock.calls).toHaveLength(3);
  });
  test('should perform a function for each element of the array', () => {
    const mockCallback = jest.fn();
    arr.forEach(mockCallback);
    expect(mockCallback.mock.calls).toHaveLength(arr.length);
  });
  test('callback should have 3 arguments (value, index, array)', () => {
    const mockCallback = jest.fn();
    arr.forEach(mockCallback);
    expect(mockCallback).toBeCalledWith(arr[0], 0, arr);
  });
  test('should take 1 required argument', () => {
    expect(arr.forEach).toHaveLength(1);
  });
  test('should not work with items added "in-progress" of callback invocation.', () => {
    const len = arr.length;
    const mockCallback = jest.fn(el => {
      arr.push(3);

      if (arr.length >= len * 2) {
        throw new Error('Test fell into recursive call of callback function');
      }
      return el > 2;
    });
    arr.forEach(mockCallback);
    expect(mockCallback.mock.calls).toHaveLength(len);
  });
  test('Callback function should use "optional this", if it\'s provided as a second argument', () => {
    const mockFn = jest.fn();
    const optionalThis = { a: 1 };
    arr.forEach(mockFn.mockReturnThis(), optionalThis);
    expect(mockFn).toHaveReturnedWith(optionalThis);
  });
  test('instance should not have Own Property forEach', () => {
    expect(Object.prototype.hasOwnProperty.call(arr, 'forEach')).toBeFalsy();
  });
  test('result array should be instance of array class', () => {
    expect(arr.forEach).toBeInstanceOf(Function);
  });
});
